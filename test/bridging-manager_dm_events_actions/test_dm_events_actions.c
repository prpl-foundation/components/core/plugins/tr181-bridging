/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>
#include <amxb/amxb_register.h>

#include "../common/common_functions.h"

#include "bridging.h"
#include "bridging_utils.h"

#include "dm_bridging_bridge.h"
#include "dm_bridging_port.h"
#include "dm_bridging_vlan.h"

#include "../common/mock.h"
#include "test_dm_events_actions.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;

static const char* odl_config = "mock.odl";
static const char* odl_defs = "../../odl/tr181-bridging_definition.odl";
static const char* odl_mock_netdev = "mock_netdev_dm.odl";
static const char* odl_mock_netmodel = "../common/mock_netmodel.odl";

#define STD_802_1D_2004         "802.1D-2004"
#define STD_802_1Q_2005         "802.1Q-2005"
#define STD_802_1Q_2011         "802.1Q-2011"

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

int test_dm_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    test_register_dummy_be();

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    resolver_add_all_functions(&parser);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    // Register data model
    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_netdev, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_netmodel, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    _bridge_main(AMXO_START, &dm, &parser);

    handle_events();

    return 0;
}

int test_dm_teardown(UNUSED void** state) {
    _bridge_main(AMXO_STOP, &dm, &parser);

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();

    return 0;
}

void test_bridge_create(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge");
    amxd_trans_add_inst(&trans, 0, "test_bridge");
    amxd_trans_set_cstring_t(&trans, "BridgeController", "mod-dummy");
    amxd_trans_set_cstring_t(&trans, "VLANController", "mod-vlan-dummy");
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "test_bridge_enabled");
    amxd_trans_set_cstring_t(&trans, "BridgeController", "mod-dummy");
    amxd_trans_set_cstring_t(&trans, "VLANController", "mod-vlan-dummy");
    amxd_trans_set_bool(&trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_port_create(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port");
    amxd_trans_add_inst(&trans, 0, "bridge");
    amxd_trans_set_cstring_t(&trans, "Name", "br-lan");
    amxd_trans_set_bool(&trans, "ManagementPort", true);
    amxd_trans_set_bool(&trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "ETH0");
    amxd_trans_set_cstring_t(&trans, "LowerLayers", "Device.something.");
    amxd_trans_set_bool(&trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "NO_NETDEV_LINK");
    amxd_trans_set_cstring_t(&trans, "Name", "no_netdev_link");
    amxd_trans_select_pathf(&trans, ".^");
    set_query_getResult_value("eth0");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

#if 0
// This scenario is not supported using netmodel queries for lowerlayers
void test_managementport(UNUSED void** state) {
    amxd_trans_t trans;

    // should not be allowed since ETH0 is the management port
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port.bridge");
    amxd_trans_set_bool(&trans, "ManagementPort", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    amxd_trans_clean(&trans);
    handle_events();

    // Remove management port attribute for ETH0
    // should also cover the port change case for Management port to false
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port.ETH0");
    amxd_trans_set_bool(&trans, "ManagementPort", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // BRIDGE can now become the management port
    // should also cover the port change case for Management port to true
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port.bridge");
    amxd_trans_set_bool(&trans, "ManagementPort", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}
#endif

void test_bridge_change(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;

    obj = amxd_dm_findf(&dm, "Bridging.Bridge.test_bridge");
    assert_int_equal(amxd_object_get_value(bool, obj, "Enable", NULL), 0);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge");
    amxd_trans_set_bool(&trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_bridge_standard_change(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_uint32_t(&trans, "MaxQBridgeEntries", 1);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.1");
    amxd_trans_set_cstring_t(&trans, "Standard", "802.1Q-2005");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // second bridge should not be allowed to change to a Q bridge
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.2");
    amxd_trans_set_cstring_t(&trans, "Standard", "802.1Q-2005");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_uint32_t(&trans, "MaxDBridgeEntries", 1);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // should not be allowed to change back to a D bridge
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.1");
    amxd_trans_set_cstring_t(&trans, "Standard", "802.1D-2004");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    amxd_trans_clean(&trans);
    handle_events();

}

void test_max_dentries(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;
    obj = amxd_dm_findf(&dm, "Bridging.Bridge");
    amxc_llist_t entries;
    amxc_llist_init(&entries);

    amxd_object_resolve_pathf(obj, &entries, ".[Standard == '%s'].", STD_802_1D_2004);
    uint32_t dbridge_entries = amxc_llist_size(&entries);
    amxc_llist_clean(&entries, amxc_string_list_it_free);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge");
    amxd_trans_add_inst(&trans, 0, "dbridge");
    amxd_trans_set_cstring_t(&trans, "Standard", "802.1D-2004");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_uint32_t(&trans, "MaxDBridgeEntries", dbridge_entries + 1);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge");
    amxd_trans_add_inst(&trans, 0, "dbridge");
    amxd_trans_set_cstring_t(&trans, "Standard", "802.1D-2004");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

}

void test_max_qentries(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;
    obj = amxd_dm_findf(&dm, "Bridging.Bridge");
    amxc_llist_t entries;
    amxc_llist_init(&entries);

    amxd_object_resolve_pathf(obj, &entries, ".[(Standard == '%s' || Standard == '%s')].",
                              STD_802_1Q_2005, STD_802_1Q_2011);
    uint32_t qbridge_entries = amxc_llist_size(&entries);
    amxc_llist_clean(&entries, amxc_string_list_it_free);

    // 802.1Q-2005
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge");
    amxd_trans_add_inst(&trans, 0, "qbridge2005");
    amxd_trans_set_cstring_t(&trans, "Standard", STD_802_1Q_2005);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_uint32_t(&trans, "MaxQBridgeEntries", qbridge_entries + 1);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge");
    amxd_trans_add_inst(&trans, 0, "qbridge2005");
    amxd_trans_set_cstring_t(&trans, "Standard", STD_802_1Q_2005);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    //802.1Q-2011
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge");
    amxd_trans_add_inst(&trans, 0, "qbridge2011");
    amxd_trans_set_cstring_t(&trans, "Standard", STD_802_1Q_2011);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.");
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_uint32_t(&trans, "MaxQBridgeEntries", qbridge_entries + 2);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge");
    amxd_trans_add_inst(&trans, 0, "qbridge2011");
    amxd_trans_set_cstring_t(&trans, "Standard", STD_802_1Q_2011);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

}

void test_bridge_status(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;
    char* bridge_status = NULL;

    obj = amxd_dm_findf(&dm, "Bridging.Bridge.test_bridge");
    assert_int_equal(amxd_object_get_value(bool, obj, "Enable", NULL), 1);
    bridge_status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(bridge_status, "Enabled");
    free(bridge_status);

    // The management port needs to be enabled for the bridge to be Enabled
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port.[ManagementPort == true]");
    amxd_trans_set_bool(&trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    bridge_status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(bridge_status, "Error");
    free(bridge_status);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port.[ManagementPort == true]");
    amxd_trans_set_bool(&trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    bridge_status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(bridge_status, "Enabled");
    free(bridge_status);

    // if the bridge is disabled the Status should go to disabled
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge");
    amxd_trans_set_bool(&trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    bridge_status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(bridge_status, "Disabled");
    free(bridge_status);
}

void test_port_status(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;
    amxd_object_t* bridge_obj = NULL;
    char* bridge_status = NULL;

    // Set bridge down for the start of the test
    bridge_obj = amxd_dm_findf(&dm, "Bridging.Bridge.test_bridge");
    assert_int_equal(amxd_object_get_value(bool, bridge_obj, "Enable", NULL), 0);
    bridge_status = amxd_object_get_value(cstring_t, bridge_obj, "Status", NULL);
    assert_string_equal(bridge_status, "Disabled");
    free(bridge_status);

    // Bridge is down so the port status should be down as well
    obj = amxd_dm_findf(&dm, "Bridging.Bridge.test_bridge.Port.ETH0.");
    char* intf_status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(intf_status, "Down");
    free(intf_status);

    // Check port Enable parameter
    assert_true(amxd_object_get_value(bool, obj, "Enable", NULL));

    // Set netdev status to up, this will be the initial state
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.1.");
    amxd_trans_set_cstring_t(&trans, "State", "up");
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port.ETH0.");
    amxd_trans_set_bool(&trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // If the bridge is disabled, the port status should not react to netdev State changes
    intf_status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(intf_status, "Down");
    free(intf_status);

    // Enable the bridge or else the port status will not come up
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge");
    amxd_trans_set_bool(&trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // make sure the bridge status is enabled or else the port status will remain down
    assert_true(amxd_object_get_value(bool, bridge_obj, "Enable", NULL));
    bridge_status = amxd_object_get_value(cstring_t, bridge_obj, "Status", NULL);
    assert_string_equal(bridge_status, "Enabled");
    free(bridge_status);

    // Initial intf state should be set after the bridge is enabled
    intf_status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(intf_status, "Up");
    free(intf_status);

    // Change the netdev interface to down
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.1.");
    amxd_trans_set_cstring_t(&trans, "State", "down");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // Port status should follow
    intf_status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_string_equal(intf_status, "Down");
    free(intf_status);
}

void test_port_add_enabled(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* bridge_obj = NULL;
    char* bridge_status = NULL;

    bridge_obj = amxd_dm_findf(&dm, "Bridging.Bridge.test_bridge.");
    assert_true(amxd_object_get_value(bool, bridge_obj, "Enable", NULL));
    bridge_status = amxd_object_get_value(cstring_t, bridge_obj, "Status", NULL);
    assert_string_equal(bridge_status, "Enabled");
    free(bridge_status);

    // Adds an enabled port to an enabled bridge
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port.");
    amxd_trans_add_inst(&trans, 0, "ETH1");
    amxd_trans_set_cstring_t(&trans, "Name", "eth1");
    amxd_trans_set_cstring_t(&trans, "LowerLayers", "Device.test_intf.eth1.");
    amxd_trans_set_bool(&trans, "Enable", true);
    set_query_getResult_value("");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_enable_stp(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* bridge_obj = NULL;
    char* bridge_status = NULL;
    char* status = NULL;

    bridge_obj = amxd_dm_findf(&dm, "Bridging.Bridge.test_bridge.");
    assert_true(amxd_object_get_value(bool, bridge_obj, "Enable", NULL));
    bridge_status = amxd_object_get_value(cstring_t, bridge_obj, "Status", NULL);
    assert_string_equal(bridge_status, "Enabled");
    free(bridge_status);

    // Enable STP
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.STP.");
    amxd_trans_set_bool(&trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
    status = amxd_object_get_value(cstring_t, bridge_obj, "Status", NULL);
    assert_string_equal("Enabled", status);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.");
    amxd_trans_set_uint32_t(&trans, "AgingTime", 40);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    free(status);
}

void test_invalid_bridge(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* bridge_obj = NULL;
    char* bridge_status = NULL;

    // Make sure the bridge is Enable before making it invalid
    bridge_obj = amxd_dm_findf(&dm, "Bridging.Bridge.test_bridge.");
    assert_int_equal(amxd_object_get_value(bool, bridge_obj, "Enable", NULL), 1);
    bridge_status = amxd_object_get_value(cstring_t, bridge_obj, "Status", NULL);
    assert_string_equal(bridge_status, "Enabled");
    free(bridge_status);

    // Remove management port attribute for ETH0
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port.[ManagementPort == true]");
    amxd_trans_set_bool(&trans, "ManagementPort", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // Bridge should now be in an ERROR state
    assert_int_equal(amxd_object_get_value(bool, bridge_obj, "Enable", NULL), 1);
    bridge_status = amxd_object_get_value(cstring_t, bridge_obj, "Status", NULL);
    assert_string_equal(bridge_status, "Error");
    free(bridge_status);

    // Set the managementport again
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port.bridge");
    amxd_trans_set_bool(&trans, "ManagementPort", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // Bridge should now be Enabled again
    assert_int_equal(amxd_object_get_value(bool, bridge_obj, "Enable", NULL), 1);
    bridge_status = amxd_object_get_value(cstring_t, bridge_obj, "Status", NULL);
    assert_string_equal(bridge_status, "Enabled");
    free(bridge_status);

    // Disable management port attribute for ETH0
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port.[ManagementPort == true]");
    amxd_trans_set_bool(&trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // Bridge should now be in an ERROR state
    assert_int_equal(amxd_object_get_value(bool, bridge_obj, "Enable", NULL), 1);
    bridge_status = amxd_object_get_value(cstring_t, bridge_obj, "Status", NULL);
    assert_string_equal(bridge_status, "Error");
    free(bridge_status);

    // Enable the managementport again
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port.[ManagementPort == true]");
    amxd_trans_set_bool(&trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // Bridge should now be Enabled again
    assert_int_equal(amxd_object_get_value(bool, bridge_obj, "Enable", NULL), 1);
    bridge_status = amxd_object_get_value(cstring_t, bridge_obj, "Status", NULL);
    assert_string_equal(bridge_status, "Enabled");
    free(bridge_status);
}

void test_lowerlayers_edit(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    // Select the management port and try to mod LowerLayer, should return amxd_status_read_only
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port.bridge");
    amxd_trans_set_cstring_t(&trans, "LowerLayers", "Device.something.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_read_only);
    amxd_trans_clean(&trans);
    handle_events();

    //Change management port to false, should return amxd_status_ok
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port.bridge");
    amxd_trans_set_bool(&trans, "ManagementPort", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    //Change LowerLayers with management port to false, should return amxd_status_ok
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port.bridge");
    amxd_trans_set_cstring_t(&trans, "LowerLayers", "Device.somethingSomething.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // Putting back the port to the management state, should return amxd_status_ok
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port.bridge");
    amxd_trans_set_bool(&trans, "ManagementPort", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // Making sure that the LowerLayers is back to read-only, should return amxd_status_read_only
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port.bridge");
    amxd_trans_set_cstring_t(&trans, "LowerLayers", "Device.something.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_read_only);
    amxd_trans_clean(&trans);
    handle_events();

    //Change LowerLayers on a non managementPort (ETH0), should return amxd_status_ok
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port.ETH0");
    amxd_trans_set_cstring_t(&trans, "LowerLayers", "Device.somethingSomething.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_port_remove(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.test_bridge.Port");
    amxd_trans_del_inst(&trans, 0, "bridge");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_bridge_remove(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge");
    amxd_trans_del_inst(&trans, 0, "test_bridge");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_wrong_reasons(UNUSED void** state) {
    assert_int_equal(_bridging_bridge_removed(NULL, NULL, 99, NULL, NULL, NULL), amxd_status_invalid_action);
    assert_int_equal(_bridging_port_removed(NULL, NULL, 99, NULL, NULL, NULL), amxd_status_invalid_action);
    assert_int_equal(_bridging_check_management_port(NULL, NULL, 99, NULL, NULL, NULL), amxd_status_invalid_action);
    assert_int_equal(_lastchange_on_read(NULL, NULL, 99, NULL, NULL, NULL), amxd_status_invalid_action);
}
