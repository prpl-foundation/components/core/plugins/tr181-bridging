/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>
#include <amxb/amxb_register.h>

#include "../common/common_functions.h"

#include "bridging.h"
#include "bridging_utils.h"

#include "dm_bridging_bridge.h"
#include "dm_bridging_port.h"
#include "dm_bridging_vlan.h"

#include "../common/mock.h"
#include "test_dm_vlan.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;

static const char* odl_config = "mock.odl";
static const char* odl_defs = "../../odl/tr181-bridging_definition.odl";
static const char* odl_mock_netmodel = "../common/mock_netmodel.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

int test_dm_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    test_register_dummy_be();

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    resolver_add_all_functions(&parser);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    // Register data model
    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_netmodel, root_obj), 0);

    _bridge_main(AMXO_START, &dm, &parser);

    handle_events();

    return 0;
}

int test_dm_teardown(UNUSED void** state) {
    _bridge_main(AMXO_STOP, &dm, &parser);
    handle_events();

    handle_events();

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();

    return 0;
}

void test_vlan_add(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.voip.VLAN.");
    amxd_trans_add_inst(&trans, 0, "vlan1");
    amxd_trans_set_bool(&trans, "Enable", true);
    amxd_trans_set_uint32_t(&trans, "VLANID", 5);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "vlan2");
    amxd_trans_set_bool(&trans, "Enable", true);
    amxd_trans_set_uint32_t(&trans, "VLANID", 10);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_vlanport_add(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.voip.VLANPort.");
    amxd_trans_add_inst(&trans, 0, "vp1");
    amxd_trans_set_cstring_t(&trans, "Name", "vlan1");
    amxd_trans_set_bool(&trans, "Enable", true);
    amxd_trans_set_cstring_t(&trans, "Port", "Device.Bridging.Bridge.1.Port.2");
    amxd_trans_set_cstring_t(&trans, "VLAN", "Device.Bridging.Bridge.1.VLAN.1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_vlanid_changed(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.voip.VLAN.vlan1.");
    amxd_trans_set_uint32_t(&trans, "VLANID", 6);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_port_type_changed(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.1.Port.2.");
    amxd_trans_set_cstring_t(&trans, "Type", "VLANUnawarePort");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.1.Port.2.");
    amxd_trans_set_cstring_t(&trans, "Type", "CustomerVLANPort");
    amxd_trans_set_cstring_t(&trans, "AcceptableFrameTypes", "AdmitOnlyVLANTagged");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_default_priority_changed(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.1.Port.2.");
    amxd_trans_set_uint32_t(&trans, "DefaultUserPriority", 6);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_vlanport_changed(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.voip.VLANPort.vp1.");
    amxd_trans_set_cstring_t(&trans, "Port", "Device.Bridging.Bridge.1.Port.3");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.voip.VLANPort.vp1.");
    amxd_trans_set_cstring_t(&trans, "VLAN", "Device.Bridging.Bridge.1.VLAN.2");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_vlanport_enable(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.voip.VLANPort.vp1.");
    amxd_trans_set_bool(&trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.voip.VLANPort.vp1.");
    amxd_trans_set_bool(&trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_bridge_toggle(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_object_t* obj = amxd_dm_findf(&dm, "Bridging.Bridge.voip.");
    char* bridge_status = amxd_object_get_value(cstring_t, obj, "Status", NULL);
    assert_non_null(bridge_status);
    assert_string_equal(bridge_status, "Enabled");
    free(bridge_status);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.voip.");
    amxd_trans_set_bool(&trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.voip.");
    amxd_trans_set_bool(&trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_config_is_valid(UNUSED void** state) {
    amxd_object_t* obj = amxd_dm_findf(&dm, "Bridging.Bridge.voip.VLANPort.vp1");
    assert_non_null(obj);
    assert_non_null(obj->priv);
    vlanport_info_t* vlanport = (vlanport_info_t*) obj->priv;

    assert_true(vlanport_config_valid(vlanport));
}

void test_vlanport_rename(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.voip.VLANPort.vp1.");
    amxd_trans_set_cstring_t(&trans, "Name", "vlan10");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

}

void test_port_remove(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* vlanport_obj = amxd_dm_findf(&dm, "Bridging.Bridge.voip.VLANPort.vp1.");
    char* port_path = NULL;

    port_path = amxd_object_get_value(cstring_t, vlanport_obj, "Port", NULL);
    assert_non_null(port_path);
    assert_string_equal(port_path, "Device.Bridging.Bridge.1.Port.3");
    free(port_path);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.voip.Port.");
    amxd_trans_del_inst(&trans, 3, NULL);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    port_path = amxd_object_get_value(cstring_t, vlanport_obj, "Port", NULL);
    assert_non_null(port_path);
    assert_string_equal(port_path, "");
    assert_false(amxd_object_get_value(bool, vlanport_obj, "Enable", NULL));
    free(port_path);
}

void test_vlan_remove(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* vlanport_obj = amxd_dm_findf(&dm, "Bridging.Bridge.voip.VLANPort.vp1.");
    char* port_path = NULL;

    port_path = amxd_object_get_value(cstring_t, vlanport_obj, "VLAN", NULL);
    assert_non_null(port_path);
    assert_string_equal(port_path, "Device.Bridging.Bridge.1.VLAN.2");
    free(port_path);

    // reenable the vlanport
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.voip.VLANPort.vp1.");
    amxd_trans_set_bool(&trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.voip.VLAN.");
    amxd_trans_del_inst(&trans, 2, NULL);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    port_path = amxd_object_get_value(cstring_t, vlanport_obj, "VLAN", NULL);
    assert_non_null(port_path);
    assert_string_equal(port_path, "");
    assert_false(amxd_object_get_value(bool, vlanport_obj, "Enable", NULL));
    free(port_path);
}

void test_vlanport_remove(UNUSED void** state) {
    amxd_trans_t trans;

    // reenable the vlanport
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.voip.VLANPort.vp1.");
    amxd_trans_set_bool(&trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Bridging.Bridge.voip.VLANPort.");
    amxd_trans_del_inst(&trans, 0, "vp1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

}

void test_wrong_reasons(UNUSED void** state) {
    assert_int_equal(_bridging_vlan_removed(NULL, NULL, 99, NULL, NULL, NULL), amxd_status_invalid_action);
    assert_int_equal(_bridging_vlanport_removed(NULL, NULL, 99, NULL, NULL, NULL), amxd_status_invalid_action);
}
