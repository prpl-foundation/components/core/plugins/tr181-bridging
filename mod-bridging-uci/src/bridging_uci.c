/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>

#include <amxm/amxm.h>

#include "bridging_uci.h"
#include "bridging_uci_utils.h"
#include "bridging_uci_commit.h"

static int add_bridge(UNUSED const char* function_name,
                      amxc_var_t* args,
                      amxc_var_t* ret) {
    int rv = -1;
    amxc_var_t uci_args;
    amxc_var_t* values = NULL;
    char* bridge_name = get_bridge_name(GET_CHAR(args, "bridge_name"));

    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    when_str_empty(bridge_name, exit);

    amxc_var_add_key(cstring_t, &uci_args, "config", NETWORK_CONFIG);
    amxc_var_add_key(cstring_t, &uci_args, "type", "interface");
    amxc_var_add_key(cstring_t, &uci_args, "name", bridge_name);
    values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
    amxc_var_add_key(cstring_t, values, "type", "bridge");
    // Needed so the empty bridge shows up in NetDev and the bridging manager can subscribe to it
    amxc_var_add_key(bool, values, "bridge_empty", true);
    amxc_var_add_key(bool, values, "disabled", !GETP_BOOL(args, "parameters.Enable"));
    amxc_var_add_key(cstring_t, &uci_args, "ifname", "");

    rv = uci_call("add", &uci_args, ret);

exit:
    amxc_var_clean(&uci_args);
    free(bridge_name);
    return rv;
}

static int set_bridge_disabled(amxc_var_t* args,
                               amxc_var_t* ret,
                               bool disabled) {
    int rv = -1;
    amxc_var_t uci_args;
    amxc_var_t* values = NULL;
    char* bridge_name = get_bridge_name(GET_CHAR(args, "bridge_name"));

    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    when_str_empty(bridge_name, exit);

    amxc_var_add_key(cstring_t, &uci_args, "config", NETWORK_CONFIG);
    amxc_var_add_key(cstring_t, &uci_args, "section", bridge_name);
    values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
    amxc_var_add_key(bool, values, "disabled", disabled);

    rv = uci_call("set", &uci_args, ret);

exit:
    amxc_var_clean(&uci_args);
    free(bridge_name);
    return rv;
}

static int enable_bridge(UNUSED const char* function_name,
                         amxc_var_t* args,
                         amxc_var_t* ret) {
    return set_bridge_disabled(args, ret, false);
}

static int disable_bridge(UNUSED const char* function_name,
                          amxc_var_t* args,
                          amxc_var_t* ret) {
    return set_bridge_disabled(args, ret, true);
}

static int remove_bridge(UNUSED const char* function_name,
                         amxc_var_t* args,
                         amxc_var_t* ret) {
    int rv = -1;
    amxc_var_t uci_args;
    char* bridge_name = get_bridge_name(GET_CHAR(args, "bridge_name"));

    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    when_str_empty(bridge_name, exit);

    amxc_var_add_key(cstring_t, &uci_args, "config", NETWORK_CONFIG);
    amxc_var_add_key(cstring_t, &uci_args, "section", bridge_name);

    rv = uci_call("delete", &uci_args, ret);

exit:
    amxc_var_clean(&uci_args);
    free(bridge_name);
    return rv;
}

static int add_port(UNUSED const char* function_name,
                    amxc_var_t* args,
                    amxc_var_t* ret) {
    int rv = -1;
    char* bridge_name = get_bridge_name(GET_CHAR(args, "bridge_name"));
    const char* port_name = GETP_CHAR(args, "port_name");

    if(GETP_BOOL(args, "parameters.ManagementPort")) {
        SAH_TRACEZ_INFO(ME, "not adding management port interface in UCI config");
        rv = 0;
        goto exit;
    }

    when_str_empty_trace(bridge_name, exit, ERROR, "no bridge name provided");

    if(GETP_BOOL(args, "parameters.IsWireless")) {
        rv = add_wireless_port(args, bridge_name, ret);
    } else {
        when_str_empty_trace(port_name, exit, ERROR, "no port name provided");
        rv = uci_ifname_set(true, port_name, bridge_name, ret);
    }

exit:
    free(bridge_name);
    return rv;
}

static int remove_port(UNUSED const char* function_name,
                       amxc_var_t* args,
                       amxc_var_t* ret) {
    int rv = 1;
    char* bridge_name = get_bridge_name(GET_CHAR(args, "bridge_name"));
    const char* port_name = GETP_CHAR(args, "port_name");

    when_str_empty_trace(bridge_name, exit, ERROR, "no bridge name provided");
    when_str_empty_trace(port_name, exit, ERROR, "no port name provided");

    if(GETP_BOOL(args, "parameters.IsWireless")) {
        rv = remove_wireless_port(args, ret);
    } else {
        rv = uci_ifname_set(false, port_name, bridge_name, ret);
    }

exit:
    free(bridge_name);
    return rv;
}

static AMXM_CONSTRUCTOR bridging_uci_start(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    amxm_module_register(&mod, so, MOD_BRIDGE_CTRL);
    amxm_module_add_function(mod, "add-bridge", add_bridge);
    amxm_module_add_function(mod, "enable-bridge", enable_bridge);
    amxm_module_add_function(mod, "disable-bridge", disable_bridge);
    amxm_module_add_function(mod, "remove-bridge", remove_bridge);
    amxm_module_add_function(mod, "add-port", add_port);
    amxm_module_add_function(mod, "remove-port", remove_port);

    return uci_commit_init();
}

static AMXM_DESTRUCTOR bridging_uci_stop(void) {

    uci_commit_clean();
    return 0;
}
