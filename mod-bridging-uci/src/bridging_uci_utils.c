/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>

#include <amxb/amxb.h>

#include "bridging_uci.h"
#include "bridging_uci_utils.h"
#include "bridging_uci_commit.h"

int uci_get(amxc_var_t* args, amxc_var_t* ret) {
    int rv = 0;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("uci.");

    rv = amxb_call(ctx, "uci.", "get", args, ret, 3);
    if(rv != AMXB_STATUS_OK) {
        SAH_TRACEZ_ERROR(ME, "failed to call UCI get, function returned %d", rv);
        goto exit;
    }

exit:
    return rv;
}

int uci_call(const char* method, amxc_var_t* args, amxc_var_t* ret) {
    int rv = 0;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("uci.");

    rv = amxb_call(ctx, "uci.", method, args, ret, 3);
    if(rv != AMXB_STATUS_OK) {
        SAH_TRACEZ_ERROR(ME, "failed to call UCI %s, function returned %d", method, rv);
        goto exit;
    }

    uci_commit_delayed();

exit:
    return rv;
}

int add_wireless_port(amxc_var_t* args, const char* bridge_name, amxc_var_t* ret) {
    int rv = 1;
    amxc_var_t uci_args;
    amxc_var_init(&uci_args);
    amxc_var_t* values = NULL;
    const char* section_name = GETP_CHAR(args, "parameters.WirelessSectionName");
    when_str_empty_trace(section_name, exit, ERROR, "no wireless section name provided");

    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &uci_args, "config", WIRELESS_CONFIG);
    amxc_var_add_key(cstring_t, &uci_args, "type", "wifi-iface");
    amxc_var_add_key(cstring_t, &uci_args, "section", section_name);
    values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
    amxc_var_add_key(cstring_t, values, "network", bridge_name);
    amxc_var_add_key(bool, values, "disabled", !GETP_BOOL(args, "parameters.Enable"));

    rv = uci_call("set", &uci_args, ret);

exit:
    amxc_var_clean(&uci_args);
    return rv;
}

int remove_wireless_port(amxc_var_t* args, amxc_var_t* ret) {
    int rv = 1;
    amxc_var_t* values = NULL;
    amxc_var_t uci_args;
    const char* section_name = GETP_CHAR(args, "parameters.WirelessSectionName");

    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);

    when_str_empty_trace(section_name, exit, ERROR, "no wireless section name provided");

    amxc_var_add_key(cstring_t, &uci_args, "config", WIRELESS_CONFIG);
    amxc_var_add_key(cstring_t, &uci_args, "type", "wifi-iface");
    amxc_var_add_key(cstring_t, &uci_args, "section", section_name);
    values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
    amxc_var_add_key(bool, values, "disabled", true);

    rv = uci_call("set", &uci_args, ret);

exit:
    amxc_var_clean(&uci_args);
    return rv;
}

int uci_ifname_set(bool to_add, const cstring_t port_name,
                   const cstring_t bridge_name, amxc_var_t* ret) {
    int rv = 1;
    int pos = -1;
    const cstring_t ifname = NULL;
    amxc_string_t ifname_new;
    amxc_var_t* values = NULL;
    amxc_var_t* tmp = NULL;
    amxc_var_t args;
    amxc_var_t set_args;

    amxc_var_init(&args);
    amxc_var_init(&set_args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&set_args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "config", NETWORK_CONFIG);
    amxc_var_add_key(cstring_t, &args, "section", bridge_name);
    uci_get(&args, ret);
    ifname = GETP_CHAR(ret, "0.values.ifname");

    amxc_string_init(&ifname_new, 0);
    if(ifname == NULL) {
        ifname = "";
    }
    when_null(port_name, exit);
    amxc_string_set(&ifname_new, ifname);

    pos = amxc_string_search(&ifname_new, port_name, 0);
    if(to_add) {
        if(pos >= 0) {
            SAH_TRACEZ_INFO(ME, "port %s already set for this bridge", port_name);
            rv = 0;
            goto exit;
        }
        amxc_string_appendf(&ifname_new, " %s", port_name);
    } else {
        if(pos < 0) {
            SAH_TRACEZ_INFO(ME, "port %s not set for this bridge %s", port_name, bridge_name);
            rv = 0;
            goto exit;
        }
        amxc_string_remove_at(&ifname_new, pos, strlen(port_name) + 1);
    }
    amxc_string_trim(&ifname_new, NULL);

    amxc_var_add_key(cstring_t, &set_args, "config", NETWORK_CONFIG);
    amxc_var_add_key(cstring_t, &set_args, "section", bridge_name);
    values = amxc_var_add_key(amxc_htable_t, &set_args, "values", NULL);
    tmp = amxc_var_add_new_key(values, "ifname");
    amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&ifname_new));

    rv = uci_call("set", &set_args, ret);

exit:
    amxc_string_clean(&ifname_new);
    amxc_var_clean(&args);
    amxc_var_clean(&set_args);
    return rv;
}

char* get_bridge_name(const char* bridge_name) {
    char* return_name = NULL;
    const char* bridge_name_cut = NULL;
    amxc_string_t sbridge_name;
    amxc_llist_t llbridge_name;

    amxc_string_init(&sbridge_name, 0);
    amxc_llist_init(&llbridge_name);

    when_str_empty(bridge_name, exit);

    amxc_string_set(&sbridge_name, bridge_name);
    amxc_string_split_to_llist(&sbridge_name, &llbridge_name, '-');
    bridge_name_cut = amxc_string_get_text_from_llist(&llbridge_name, 1);
    if((bridge_name_cut != NULL) && (*bridge_name_cut != 0)) {
        return_name = strdup(bridge_name_cut);
    }

exit:
    amxc_llist_clean(&llbridge_name, amxc_string_list_it_free);
    amxc_string_clean(&sbridge_name);
    return return_name;
}
