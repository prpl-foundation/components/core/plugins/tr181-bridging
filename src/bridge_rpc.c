/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "bridging.h"
#include "bridging_utils.h"
#include "bridge_rpc.h"

#define VLAN_STANDARD "802.1Q-2011"
#define ME "bridging"

/**
 * @brief Sets the Enable parameter of the provided object
 * @param obj object pointer for which to set the enable
 * @param enable value to set
 * @return amxd_status_ok if enable was set successful, error value otherwise
 */
static amxd_status_t toggle_intf(amxd_object_t* obj, bool enable) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(bool, &trans, "Enable", enable);
    status = amxd_trans_apply(&trans, bridge_get_dm());

    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return status;
}

/**
 * @brief get the full path of an object, prefixed with the "Device" prefix
 * @param obj object pointer for which to get the device path
 * @note This function allocates memory to store the path. The memory needs to be freed using "free" if not needed anymore
 */
static char* object_get_device_path(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    char* path = NULL;
    char* return_path = NULL;
    amxc_string_t device_path;
    amxc_string_init(&device_path, 0);

    path = amxd_object_get_path(obj, AMXD_OBJECT_INDEXED);
    when_str_empty(path, exit);
    amxc_string_setf(&device_path, "Device.%s", path);
    return_path = amxc_string_take_buffer(&device_path);

exit:
    free(path);
    amxc_string_clean(&device_path);
    SAH_TRACEZ_OUT(ME);
    return return_path;
}

/**
 * @brief Looks for a port instance with a matching LowerLayer and type, can create one if not found
 * @param create when true, a port instance is created if a matching one is not found
 * @param bridge_obj bridge object where to search
 * @param bridge_path path to the bridge instance
 * @param alias optional: alias for the port
 * @param lowerlayers Device path for the lower layer interface
 * @param vlan_capable if true, function will look for a Port with type CustomerVLANPort (VLANUnawarePort otherwise) or will create one with this type
 * If true the Bridge standard is also set to "802.1Q-2011"
 * @return vlan instance object if found/created, NULL otherwise
 */
static amxd_object_t* get_port_instance(bool create,
                                        amxd_object_t* bridge_obj,
                                        const char* bridge_path,
                                        const char* alias,
                                        const char* lowerlayers,
                                        bool vlan_capable) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    const char* type = vlan_capable ? "CustomerVLANPort" : "VLANUnawarePort";
    amxd_object_t* port_obj = NULL;

    when_str_empty_trace(lowerlayers, exit, ERROR, "LowerLayers should not be empty");
    port_obj = amxd_object_findf(bridge_obj, ".Port.[LowerLayers == '%s' && Type == '%s']",
                                 lowerlayers, type);

    if(create) {
        amxd_trans_t trans;
        amxd_trans_init(&trans);

        if((port_obj == NULL)) {
            amxd_trans_select_pathf(&trans, "%s.Port.", bridge_path);
            amxd_trans_add_inst(&trans, 0, alias);
            amxd_trans_set_value(cstring_t, &trans, "LowerLayers", lowerlayers);
        } else if(vlan_capable) {
            amxd_trans_select_object(&trans, port_obj);
        }

        if(vlan_capable) {
            amxd_trans_set_value(cstring_t, &trans, "AcceptableFrameTypes", "AdmitOnlyVLANTagged");
            amxd_trans_set_value(cstring_t, &trans, "Type", "CustomerVLANPort");
            amxd_trans_select_object(&trans, bridge_obj);
            amxd_trans_set_value(cstring_t, &trans, "Standard", VLAN_STANDARD);
        }

        status = amxd_trans_apply(&trans, bridge_get_dm());
        when_failed_trace(status, create_exit, ERROR, "Failed to create new port instance, status '%d'", status);

        if(port_obj == NULL) {
            port_obj = amxd_object_findf(bridge_obj,
                                         ".Port.[LowerLayers == '%s' && Type == '%s']",
                                         lowerlayers, type);
        }
create_exit:
        amxd_trans_clean(&trans);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return port_obj;
}

/**
 * @brief Looks for a VLANP instance with a matching VLANID, can create one if not found
 * @param create when true, a vlan instance is created if a matching one is not found
 * @param bridge_obj bridge object where to search
 * @param bridge_path path to the bridge instance
 * @param vlan_id id used to search and optionally create the vlan
 * @return vlan instance object if found/created, NULL otherwise
 */
static amxd_object_t* get_vlan_instance(bool create,
                                        amxd_object_t* bridge_obj,
                                        const char* bridge_path,
                                        uint32_t vlan_id) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* vlan_obj = amxd_object_findf(bridge_obj,
                                                ".VLAN.[VLANID == '%d']",
                                                vlan_id);

    if((vlan_obj == NULL) && create) {
        amxd_status_t status = amxd_status_unknown_error;
        amxd_trans_t trans;
        amxd_trans_init(&trans);
        amxd_trans_select_pathf(&trans, "%s.VLAN.", bridge_path);
        amxd_trans_add_inst(&trans, 0, NULL);
        amxd_trans_set_value(uint32_t, &trans, "VLANID", vlan_id);
        status = amxd_trans_apply(&trans, bridge_get_dm());
        amxd_trans_clean(&trans);

        when_failed_trace(status, exit, ERROR, "Failed to create new vlan instance, status '%d'", status);
        vlan_obj = amxd_object_findf(bridge_obj,
                                     ".VLAN.[VLANID == '%d']",
                                     vlan_id);
        when_null_trace(vlan_obj, exit, ERROR, "Failed to find newly added vlan with VLANID '%d'", vlan_id);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return vlan_obj;
}

/**
 * @brief Looks for a VLANPort instance with a matching Port and VLAN, can create one if not found
 * @param create when true, a vlanport instance is created if a matching one is not found
 * @param bridge_obj bridge object where to search
 * @param bridge_path path to the bridge instance
 * @param port_path Device path to the port instance
 * @param vlan_path Device path to the vlan instance
 * @param vlan_name Optional: User friendly name for the vlan interface
 * @return vlanport instance object if found/created, NULL otherwise
 */
static amxd_object_t* get_vlanport_instance(bool create,
                                            amxd_object_t* bridge_obj,
                                            const char* bridge_path,
                                            const char* port_path,
                                            const char* vlan_path,
                                            const char* vlan_name,
                                            int vlan_prio) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* vlanport_obj = amxd_object_findf(bridge_obj,
                                                    ".VLANPort.[Port == '%s' && VLAN == '%s']",
                                                    port_path,
                                                    vlan_path);
    if((vlanport_obj == NULL) && create) {
        amxd_status_t status = amxd_status_unknown_error;
        amxd_trans_t trans;

        amxd_trans_init(&trans);
        amxd_trans_select_pathf(&trans, "%s.VLANPort.", bridge_path);
        amxd_trans_add_inst(&trans, 0, NULL);
        amxd_trans_set_value(cstring_t, &trans, "VLAN", vlan_path);
        amxd_trans_set_value(cstring_t, &trans, "Port", port_path);
        amxd_trans_set_value(int32_t, &trans, "VlanPriority", vlan_prio);
        if(!str_empty(vlan_name)) {
            amxd_trans_set_value(cstring_t, &trans, "Name", vlan_name);
        }
        status = amxd_trans_apply(&trans, bridge_get_dm());
        amxd_trans_clean(&trans);

        when_failed_trace(status, exit, ERROR, "Failed to create new vlan instance, status '%d'", status);
        vlanport_obj = amxd_object_findf(bridge_obj,
                                         ".VLANPort.[Port == '%s' && VLAN == '%s']",
                                         port_path, vlan_path);
        when_null_trace(vlanport_obj, exit, ERROR, "Failed to find newly added vlanport");
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return vlanport_obj;
}

/**
 * @brief Enables the port and vlan instance so they can be use for vlans
 * @param port_obj the port object that should be enabled
 * @param vlan_obj the vlan object that should be enabled
 * @return amxd_status_ok if successful, error otherwise
 */
static amxd_status_t enable_sub_instances(amxd_object_t* port_obj,
                                          amxd_object_t* vlan_obj) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    amxd_trans_select_object(&trans, port_obj);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_object(&trans, vlan_obj);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    status = amxd_trans_apply(&trans, bridge_get_dm());

    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return status;
}


/**
 * @brief function to add/remove (vlan)ports in the bridge manager
 * @param create if set to true, a (vlan)port will be enabled/created, if false the (vlan)port will be disabled
 * @param bridge_obj object pointer for the bridge on which to add the (vlan)port
 * @param args variant containing the needed values
 * @return amxd_status_ok if successful, error otherwise
 */
static amxd_status_t manage_port(bool create, amxd_object_t* bridge_obj, amxc_var_t* args) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    uint32_t vlan_id = GET_UINT32(args, "VlanId");
    char* bridge_path = NULL;
    amxd_object_t* port_obj = NULL;
    amxd_object_t* intf_obj = NULL;
    bool vlan_capable = (GET_ARG(args, "VlanId") != NULL);
    char* lowerlayers = trim_final_dot(GET_CHAR(args, "LowerLayers"));

    when_str_empty_trace(lowerlayers, exit, ERROR, "No lowerlayer provided");

    if(create) {
        bridge_path = amxd_object_get_path(bridge_obj, AMXD_OBJECT_INDEXED);
        when_str_empty_trace(bridge_path, exit, ERROR, "Failed to get the bridge path");
    }

    port_obj = get_port_instance(create, bridge_obj, bridge_path, GET_CHAR(args, "Alias"), lowerlayers, vlan_capable);
    when_null_trace(port_obj, exit, ERROR, "No matching port object found and/or failed to create");
    if(vlan_capable) {
        amxd_object_t* vlan_obj = get_vlan_instance(create, bridge_obj, bridge_path, vlan_id);
        char* vlan_path = object_get_device_path(vlan_obj);
        char* port_path = object_get_device_path(port_obj);
        status = amxd_status_unknown_error;

        when_str_empty_trace(vlan_path, exit, ERROR, "Failed to find vlan path for vlan id '%d'", vlan_id);
        when_str_empty_trace(port_path, exit, ERROR, "Failed to find port path for LowerLayers '%s'", lowerlayers);

        // For vlans, the enable from the vlanport controls the interface
        intf_obj = get_vlanport_instance(create, bridge_obj, bridge_path, port_path,
                                         vlan_path, GET_CHAR(args, "VlanName"),
                                         GET_INT32(args, "VlanPriority"));
        when_null_trace(intf_obj, vlan_exit, ERROR, "Failed to get vlanport instance");

        // in case we are not creating a vlan, we do not need to configure it
        when_false_status(create, vlan_exit, status = amxd_status_ok);
        status = enable_sub_instances(port_obj, vlan_obj);

vlan_exit:
        free(port_path);
        free(vlan_path);
        when_failed_trace(status, exit, ERROR, "Failed to configure bridge/port for use with vlans, status '%d'", status);
    } else {
        intf_obj = port_obj;         // For non vlan interfaces, the port controls the interface
    }

    status = toggle_intf(intf_obj, GET_BOOL(args, "Enable"));

exit:
    free(lowerlayers);
    free(bridge_path);
    SAH_TRACEZ_OUT(ME);
    return status;
}

/*
 * Adds a new port to the bridge.
 * Depending on the parameters, the port will be a normal interface or a vlan
 * If a port with the requested lowerlayer already exists, this one will be used, the requested alias will be ignored
 * Defining a vlan will switch the bridge standard to "802.1Q-2011"
 */
amxd_status_t _AddPort(amxd_object_t* bridge_obj, UNUSED amxd_function_t* func, amxc_var_t* args, UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = manage_port(true, bridge_obj, args);

    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _DisablePort(amxd_object_t* bridge_obj, UNUSED amxd_function_t* func, amxc_var_t* args, UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = manage_port(false, bridge_obj, args);

    SAH_TRACEZ_OUT(ME);
    return status;
}
