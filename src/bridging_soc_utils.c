/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <net/if.h>
#include <errno.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "bridging.h"
#include "bridging_soc_utils.h"

#define ME "soc_utils"

static const char* port_get_bridge_ctrl(amxd_object_t* port) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* port_templ = amxd_object_get_parent(port);
    amxd_object_t* bridge = amxd_object_get_parent(port_templ);
    const amxc_var_t* bridge_ctrl = amxd_object_get_param_value(bridge, BRIDGE_CTRL);
    const char* bridge_ctrl_name = NULL;

    if(bridge_mods_are_loaded()) {
        bridge_ctrl_name = GET_CHAR(bridge_ctrl, NULL);
    } else {
        bridge_ctrl_name = DUMMY_MOD_NAME;
    }

    SAH_TRACEZ_OUT(ME);
    return bridge_ctrl_name;
}

static const char* bridging_get_bridge_ctrl(amxd_object_t* bridge) {
    SAH_TRACEZ_IN(ME);
    const amxc_var_t* bridge_ctrl = amxd_object_get_param_value(bridge, BRIDGE_CTRL);
    const char* bridge_ctrl_name = NULL;

    if(bridge_mods_are_loaded()) {
        bridge_ctrl_name = GET_CHAR(bridge_ctrl, NULL);
    } else {
        bridge_ctrl_name = DUMMY_MOD_NAME;
    }

    SAH_TRACEZ_OUT(ME);
    return bridge_ctrl_name;
}

static const char* bridging_get_vlan_ctrl(amxd_object_t* bridge) {
    SAH_TRACEZ_IN(ME);
    const amxc_var_t* vlan_ctrl = amxd_object_get_param_value(bridge, VLAN_CTRL);
    const char* vlan_ctrl_name = NULL;

    if(bridge_vlan_mods_are_loaded()) {
        vlan_ctrl_name = GET_CHAR(vlan_ctrl, NULL);
    } else {
        vlan_ctrl_name = DUMMY_VLAN_MOD_NAME;
    }

    SAH_TRACEZ_OUT(ME);
    return vlan_ctrl_name;
}

static const char* bridging_get_stp_ctrl(amxd_object_t* bridge) {
    SAH_TRACEZ_IN(ME);
    const amxc_var_t* stp_ctrl = amxd_object_get_param_value(bridge, STP_CTRL);
    const char* stp_ctrl_name = NULL;

    stp_ctrl_name = GET_CHAR(stp_ctrl, NULL);

    SAH_TRACEZ_OUT(ME);
    return stp_ctrl_name;
}

static void mod_bridging_build_port_data_struct(port_info_t* port, amxc_var_t* data) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* params = NULL;
    bridge_info_t* bridge = (bridge_info_t*) port->bridge_obj->priv;
    when_null_trace(bridge, exit, ERROR, "no bridge found");
    when_str_empty(bridge->bridge_name, exit);
    when_str_empty(port->netdev_intf, exit);


    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, data, "bridge_name", bridge->bridge_name);
    params = amxc_var_add_key(amxc_htable_t, data, "parameters", NULL);
    amxd_object_get_params(port->port_obj, params, amxd_dm_access_protected);
    amxc_var_add_key(cstring_t, data, "port_name", GETP_CHAR(params, "Name"));

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void mod_bridging_build_vlanport_data_struct(vlanport_info_t* vlanport, amxc_var_t* data) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* params = NULL;
    bridge_info_t* bridge = (bridge_info_t*) vlanport->bridge_obj->priv;
    when_null_trace(bridge, exit, ERROR, "no bridge found");
    when_str_empty(bridge->bridge_name, exit);
    when_str_empty(vlanport->vlanport_name, exit);

    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, data, "bridge_name", bridge->bridge_name);
    params = amxc_var_add_key(amxc_htable_t, data, "parameters", NULL);
    amxd_object_get_params(vlanport->port_obj, params, amxd_dm_access_protected);
    amxc_var_add_key(cstring_t, data, "port_name", vlanport->vlanport_name);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void mod_bridging_build_bridge_data_struct(amxd_object_t* bridge_obj, amxc_var_t* data) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* params = NULL;
    bridge_info_t* bridge = (bridge_info_t*) bridge_obj->priv;
    when_null_trace(bridge, exit, ERROR, "no bridge found");
    when_str_empty(bridge->bridge_name, exit);

    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, data, "bridge_name", bridge->bridge_name);
    params = amxc_var_add_key(amxc_htable_t, data, "parameters", NULL);
    amxd_object_get_params(bridge_obj, params, amxd_dm_access_protected);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void mod_bridging_build_stp_data_struct(amxd_object_t* bridge_obj, amxc_var_t* data) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* stp_obj = amxd_object_findf(bridge_obj, ".STP.");
    bridge_info_t* bridge = NULL;

    when_null_trace(bridge_obj, exit, ERROR, "Bridge object is NULL");
    bridge = (bridge_info_t*) bridge_obj->priv;
    when_null_trace(bridge, exit, ERROR, "Bridge object private data is NULL");
    when_null_trace(stp_obj, exit, ERROR, "No STP object found");
    when_str_empty_trace(bridge->bridge_name, exit, ERROR, "Bridge name is empty, can't build STP data struct");

    amxd_object_get_params(stp_obj, data, amxd_dm_access_protected);
    amxc_var_add_key(cstring_t, data, "bridge_name", bridge->bridge_name);
    amxc_var_add_key(uint64_t, data, "AgingTime", amxd_object_get_value(uint64_t, bridge_obj, "AgingTime", NULL));

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static int mod_vlan_build_vlanport_data_struct(vlanport_info_t* vlanport, amxc_var_t* data) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t* extra_params = NULL;
    amxc_var_t* port_params = NULL;
    amxc_var_t* vlan_params = NULL;
    amxc_var_t* vlanport_params = NULL;
    int32_t egress_prio = -1;

    when_null_trace(vlanport->port_obj, exit, ERROR, "port object can not be NULL, no valid path");
    when_null_trace(vlanport->vlan_obj, exit, ERROR, "vlan object can not be NULL, no valid path");

    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    // Add the extra arguments
    extra_params = amxc_var_add_key(amxc_htable_t, data, "bridging", NULL);
    port_params = amxc_var_add_key(amxc_htable_t, extra_params, "port_params", NULL);
    amxd_object_get_params(vlanport->port_obj, port_params, amxd_dm_access_protected);
    vlan_params = amxc_var_add_key(amxc_htable_t, extra_params, "vlan_params", NULL);
    amxd_object_get_params(vlanport->vlan_obj, vlan_params, amxd_dm_access_protected);
    vlanport_params = amxc_var_add_key(amxc_htable_t, extra_params, "vlanport_params", NULL);
    amxd_object_get_params(vlanport->vlanport_obj, vlanport_params, amxd_dm_access_protected);

    // Add mandatory arguments
    egress_prio = GET_INT32(vlanport_params, "VlanPriority");
    if(egress_prio < 0) {
        amxc_var_add_key(uint32_t, data, "egress_priority", GET_UINT32(port_params, "DefaultUserPriority"));
    } else {
        amxc_var_add_key(uint32_t, data, "egress_priority", egress_prio);
    }
    amxc_var_add_key(cstring_t, data, "port_name", GET_CHAR(port_params, "Name"));
    amxc_var_add_key(cstring_t, data, "vlanport_name", vlanport->vlanport_name);
    amxc_var_add_key(int32_t, data, "vlanid", GET_INT32(vlan_params, "VLANID"));

    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int mod_bridging_execute_port_function(const char* function, port_info_t* port) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t data;
    amxc_var_t ret;
    const char* bridge_ctrl = NULL;
    int rv = -1;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    when_str_empty(function, exit);
    when_null_trace(port->port_obj, exit, ERROR, "can not execute %s, no object given", function);

    bridge_ctrl = port_get_bridge_ctrl(port->port_obj);
    SAH_TRACEZ_INFO(ME, "%s -> call implementation (controller = '%s')", function, bridge_ctrl);
    mod_bridging_build_port_data_struct(port, &data);
    rv = amxm_execute_function(bridge_ctrl, MOD_BRIDGE_CTRL, function, &data, &ret);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "function %s failed", function);
    }

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&data);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int mod_bridging_execute_vlanport_function(const char* function, vlanport_info_t* vlanport) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t data;
    amxc_var_t ret;
    const char* bridge_ctrl = NULL;
    int rv = -1;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    when_str_empty(function, exit);
    when_null_trace(vlanport->port_obj, exit, ERROR, "can not execute %s, no object given", function);

    bridge_ctrl = port_get_bridge_ctrl(vlanport->port_obj);
    SAH_TRACEZ_INFO(ME, "%s -> call implementation (controller = '%s')", function, bridge_ctrl);
    mod_bridging_build_vlanport_data_struct(vlanport, &data);
    rv = amxm_execute_function(bridge_ctrl, MOD_BRIDGE_CTRL, function, &data, &ret);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Function %s from controller %s failed", function, bridge_ctrl);
    }

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&data);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int mod_bridging_execute_bridge_function(const char* function, amxd_object_t* bridge_obj) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t data;
    amxc_var_t ret;
    const char* bridge_ctrl = NULL;
    int rv = -1;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    when_str_empty(function, exit);
    when_null_trace(bridge_obj, exit, ERROR, "can not execute %s, no object given", function);

    bridge_ctrl = bridging_get_bridge_ctrl(bridge_obj);
    SAH_TRACEZ_INFO(ME, "%s -> call implementation (controller = '%s')", function, bridge_ctrl);
    mod_bridging_build_bridge_data_struct(bridge_obj, &data);
    rv = amxm_execute_function(bridge_ctrl, MOD_BRIDGE_CTRL, function, &data, &ret);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "function %s failed", function);
    }

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&data);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int mod_bridging_execute_stp_function(const char* function, amxd_object_t* bridge_obj, amxc_var_t* data, amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    const char* bridge_ctrl = NULL;
    int rv = -1;
    bool data_created = (data == NULL);

    when_str_empty_trace(function, exit, ERROR, "Function name is empty");
    when_null_trace(bridge_obj, exit, ERROR, "can not execute %s, no object given", function);
    when_null_trace(ret, exit, ERROR, "ret variant is NULL");

    bridge_ctrl = bridging_get_stp_ctrl(bridge_obj);
    SAH_TRACEZ_INFO(ME, "%s -> call implementation (controller = '%s')", function, bridge_ctrl);
    when_str_empty_trace(bridge_ctrl, exit, ERROR, "No STP controller found");

    if(data == NULL) {
        amxc_var_new(&data);
        mod_bridging_build_stp_data_struct(bridge_obj, data);
    }

    if(strcmp(bridge_ctrl, "mod-stp-sysfs") == 0) {
        if(access("/sbin/bridge-stp", F_OK) == 0) {
            SAH_TRACEZ_ERROR(ME, "Kernel mode STP is configured but a user space STP helper exists. Conflicting configuration!");
            goto exit;
        } else if(errno != ENOENT) {
            // The file might exist but an error was encountered when trying to access it
            SAH_TRACEZ_ERROR(ME, "Error accessing file /sbin/bridge-stp: %s", strerror(errno));
        }
    }

    rv = amxm_execute_function(bridge_ctrl, MOD_STP_CTRL, function, data, ret);
    when_failed_trace(rv, exit, ERROR, "function %s failed", function);

exit:
    if(data_created) {
        amxc_var_delete(&data);
    }
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int mod_vlan_execute_function(const char* function, vlanport_info_t* vlanport) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t data;
    amxc_var_t ret;
    const char* vlanport_name = NULL;
    const char* vlan_ctrl = NULL;
    int rv = -1;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    when_str_empty(function, exit);
    when_null(vlanport, exit);
    when_null_trace(vlanport->bridge_obj, exit, ERROR, "can not execute %s, no object given", function);

    vlan_ctrl = bridging_get_vlan_ctrl(vlanport->bridge_obj);
    SAH_TRACEZ_INFO(ME, "%s -> call implementation (controller = '%s')", function, vlan_ctrl);
    when_failed(mod_vlan_build_vlanport_data_struct(vlanport, &data), exit);
    when_failed_trace(amxm_execute_function(vlan_ctrl, MOD_VLAN_CTRL, function, &data, &ret),
                      exit, ERROR, "function %s failed", function);

    vlanport_name = GETP_CHAR(&ret, "vlanport_name");
    if((vlanport_name != NULL) && (*vlanport_name != 0)) {
        vlanport->vlanport_name = strndup(vlanport_name, 16);
    }
    rv = 0;

exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}
