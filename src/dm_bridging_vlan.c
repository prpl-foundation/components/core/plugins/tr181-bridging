/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "bridging.h"
#include "dm_bridging_vlan.h"
#include "bridging_utils.h"
#include "bridging_soc_utils.h"

#define ME "vlan-bridging"

static int _vlan_vlanport_remove(UNUSED amxd_object_t* templ,
                                 amxd_object_t* instance,
                                 UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    vlanport_info_t* vlanport = NULL;
    bool enabled = amxd_object_get_value(bool, instance, "Enable", NULL);
    when_null(instance, exit);
    when_null(instance->priv, exit);
    vlanport = (vlanport_info_t*) instance->priv;

    // If the vlanport is enabled, the current config should be removed first
    if(enabled) {
        mod_bridge_vlanport_remove(vlanport);
    }
    vlanport->vlan_obj = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int _vlan_vlanport_disable(UNUSED amxd_object_t* templ,
                                  amxd_object_t* instance,
                                  UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    when_null(instance, exit);

    rv = amxd_object_set_value(bool, instance, "Enable", false);
    rv |= amxd_object_set_value(cstring_t, instance, "VLAN", "");

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int _vlan_vlanport_recreate(UNUSED amxd_object_t* templ,
                                   amxd_object_t* instance,
                                   UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    vlanport_info_t* vlanport = NULL;
    bool enabled = amxd_object_get_value(bool, instance, "Enable", NULL);

    when_null(instance, exit);
    when_null(instance->priv, exit);
    vlanport = (vlanport_info_t*) instance->priv;

    // If the vlanport is enabled, the current config should recreated
    if(enabled) {
        mod_bridge_vlanport_remove(vlanport);
        mod_bridge_vlanport_add(vlanport);
    }

    rv = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _bridging_vlan_removed(amxd_object_t* object,
                                     UNUSED amxd_param_t* param,
                                     amxd_action_t reason,
                                     UNUSED const amxc_var_t* const args,
                                     UNUSED amxc_var_t* const retval,
                                     UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;
    amxc_string_t spath;
    char* vlan_path = amxd_object_get_path(object, AMXD_OBJECT_INDEXED);

    amxc_string_init(&spath, 0);

    if(reason != action_object_destroy) {
        SAH_TRACEZ_NOTICE(ME, "Wrong reason, expected action_object_destroy(%d) got %d", action_object_destroy, reason);
        rv = amxd_status_invalid_action;
        goto exit;
    }

    when_null_trace(object, exit, ERROR, "object can not be null, invalid argument");
    when_str_empty(vlan_path, exit);

    SAH_TRACEZ_INFO(ME, "Removing vlan: %s", amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    // The remove function is also called for the vlan template object, we only want to destroy
    // the instances
    if(object->type != amxd_object_instance) {
        SAH_TRACEZ_INFO(ME, "Skipping vlan template, only remove instances of vlan");
        rv = amxd_status_ok;
        goto exit;
    }

    amxc_string_setf(&spath, ".^.^.VLANPort.[VLAN == 'Device.%s']", vlan_path);
    amxd_object_for_all(object, amxc_string_get(&spath, 0), _vlan_vlanport_remove, NULL);

    rv = amxd_status_ok;
exit:
    free(vlan_path);
    amxc_string_clean(&spath);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _bridging_vlanport_removed(amxd_object_t* object,
                                         UNUSED amxd_param_t* param,
                                         amxd_action_t reason,
                                         UNUSED const amxc_var_t* const args,
                                         UNUSED amxc_var_t* const retval,
                                         UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    vlanport_info_t* vlanport = NULL;
    amxd_status_t rv = amxd_status_invalid_function_argument;
    bool enabled = amxd_object_get_value(bool, object, "Enable", NULL);

    if(reason != action_object_destroy) {
        SAH_TRACEZ_NOTICE(ME, "Wrong reason, expected action_object_destroy(%d) got %d", action_object_destroy, reason);
        rv = amxd_status_invalid_action;
        goto exit;
    }

    when_null_trace(object, exit, ERROR, "object can not be null, invalid argument");

    SAH_TRACEZ_INFO(ME, "Removing vlanport: %s", amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    // The remove function is also called for the vlanport template object, we only want to destroy
    // the instances
    if(object->type != amxd_object_instance) {
        SAH_TRACEZ_INFO(ME, "Skipping vlanport template, only remove instances of vlanport");
        rv = amxd_status_ok;
        goto exit;
    }

    when_null_trace(object->priv, exit, WARNING, "Object has no private data, can't destroy port");
    vlanport = (vlanport_info_t*) object->priv;

    // If the vlanport is enabled, the current config should be removed before the clean
    if(enabled) {
        mod_bridge_vlanport_remove(vlanport);
    }

    rv = vlanport_info_clean(&vlanport);
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void _bridging_vlan_deleted(UNUSED const char* const sig_name,
                            const amxc_var_t* const data,
                            UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t spath;
    amxd_object_t* vlan_templ_obj = amxd_dm_signal_get_object(bridge_get_dm(), data);

    amxc_string_init(&spath, 0);

    amxc_string_setf(&spath, ".^.VLANPort.[VLAN == 'Device.%s%d']", GETP_CHAR(data, "path"), GETP_INT32(data, "index"));
    amxd_object_for_all(vlan_templ_obj, amxc_string_get(&spath, 0), _vlan_vlanport_disable, NULL);

    amxc_string_clean(&spath);
    SAH_TRACEZ_OUT(ME);
}

void _bridging_vlan_id_changed(UNUSED const char* const sig_name,
                               const amxc_var_t* const data,
                               UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t spath;
    char* trimmed_path = trim_final_dot(GET_CHAR(data, "path"));
    amxd_object_t* vlan_obj = amxd_dm_signal_get_object(bridge_get_dm(), data);

    amxc_string_init(&spath, 0);


    amxc_string_setf(&spath, ".^.VLANPort.[VLAN == 'Device.%s']", trimmed_path);
    amxd_object_for_all(amxd_object_get_parent(vlan_obj), amxc_string_get(&spath, 0), _vlan_vlanport_recreate, NULL);

    free(trimmed_path);
    amxc_string_clean(&spath);
    SAH_TRACEZ_OUT(ME);
}

void _bridging_vlanport_added(UNUSED const char* const sig_name,
                              const amxc_var_t* const data,
                              UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    vlanport_info_t* vlanport = NULL;
    amxd_object_t* vlanport_templ_obj = amxd_dm_signal_get_object(bridge_get_dm(), data);
    amxd_object_t* vlanport_obj = amxd_object_get_instance(vlanport_templ_obj, NULL, GET_UINT32(data, "index"));
    amxd_object_t* bridge_obj = amxd_object_get_parent(vlanport_templ_obj);
    amxd_object_t* port_obj = get_object_from_device_path(GETP_CHAR(data, "parameters.Port"));
    amxd_object_t* vlan_obj = get_object_from_device_path(GETP_CHAR(data, "parameters.VLAN"));
    char* vlanport_name = amxd_object_get_cstring_t(vlanport_obj, "Name", NULL);

    rv = vlanport_info_new(&vlanport, vlanport_obj, bridge_obj, port_obj, vlan_obj, vlanport_name);
    when_failed(rv, exit);

    vlanport_obj->priv = vlanport;

    if(GETP_BOOL(data, "parameters.Enable")) {
        mod_bridge_vlanport_add(vlanport);
    }
exit:
    free(vlanport_name);
    SAH_TRACEZ_OUT(ME);
    return;
}

void _bridging_vlanport_enable_changed(UNUSED const char* const sig_name,
                                       const amxc_var_t* const data,
                                       UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    vlanport_info_t* vlanport = NULL;
    amxd_object_t* vlanport_obj = amxd_dm_signal_get_object(bridge_get_dm(), data);

    when_null(vlanport_obj, exit);
    vlanport = (vlanport_info_t*) vlanport_obj->priv;
    when_null(vlanport, exit);
    when_null_trace(data, exit, ERROR, "data can not be NULL, change not applied");

    if(GETP_BOOL(data, "parameters.Enable.to")) {
        mod_bridge_vlanport_add(vlanport);
    } else {
        mod_bridge_vlanport_remove(vlanport);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _bridging_vlanport_changed(UNUSED const char* const sig_name,
                                const amxc_var_t* const data,
                                UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    vlanport_info_t* vlanport = NULL;
    amxd_object_t* vlanport_obj = amxd_dm_signal_get_object(bridge_get_dm(), data);
    bool enabled = amxd_object_get_bool(vlanport_obj, "Enable", NULL);

    when_null_trace(vlanport_obj, exit, ERROR, "could not find the vlanport object");
    vlanport = (vlanport_info_t*) vlanport_obj->priv;
    when_null(vlanport, exit);
    // If the vlanport is enabled, the current config should be removed first
    if(enabled) {
        mod_bridge_vlanport_remove(vlanport);
    }

    if(GETP_CHAR(data, "parameters.Port.to") != NULL) {
        vlanport->port_obj = get_object_from_device_path(GETP_CHAR(data, "parameters.Port.to"));
    }
    if(GETP_CHAR(data, "parameters.VLAN.to")) {
        vlanport->vlan_obj = get_object_from_device_path(GETP_CHAR(data, "parameters.VLAN.to"));
    }

    // If the vlanport is enabled, the new config should be added
    if(enabled) {
        mod_bridge_vlanport_add(vlanport);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _bridging_vlanport_renamed(UNUSED const char* const sig_name,
                                const amxc_var_t* const data,
                                UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    vlanport_info_t* vlanport = NULL;
    amxd_object_t* vlanport_obj = amxd_dm_signal_get_object(bridge_get_dm(), data);
    bool enabled = amxd_object_get_bool(vlanport_obj, "Enable", NULL);
    const char* vlanport_name = GETP_CHAR(data, "parameters.Name.to");

    when_null_trace(vlanport_name, exit, ERROR, "vlan name can not be NULL"); // an empty string is allowed
    when_null_trace(vlanport_obj, exit, ERROR, "could not find the vlanport object");
    vlanport = (vlanport_info_t*) vlanport_obj->priv;
    when_null(vlanport, exit);

    // If the vlanport is enabled, the current config should be removed first
    if(enabled) {
        mod_bridge_vlanport_remove(vlanport);
    }

    free(vlanport->vlanport_name);
    vlanport->vlanport_name = strndup(vlanport_name, 16);

    // If the vlanport is enabled, the new config should be added
    if(enabled) {
        mod_bridge_vlanport_add(vlanport);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _bridging_vlanport_priority_changed(UNUSED const char* const sig_name,
                                         const amxc_var_t* const data,
                                         UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    vlanport_info_t* vlanport = NULL;
    amxd_object_t* vlanport_obj = amxd_dm_signal_get_object(bridge_get_dm(), data);
    bool enabled = amxd_object_get_bool(vlanport_obj, "Enable", NULL);

    when_null_trace(vlanport_obj, exit, ERROR, "could not find the vlanport object");
    vlanport = (vlanport_info_t*) vlanport_obj->priv;
    when_null(vlanport, exit);

    // If the vlanport is enabled, the current config should recreated
    if(enabled) {
        mod_vlan_execute_function("set-vlan-priority", vlanport);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}
