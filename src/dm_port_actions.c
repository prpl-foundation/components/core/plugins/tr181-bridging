/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "bridging.h"
#include "bridging_utils.h"
#include "dm_bridging_port.h"

#define ME "actions"

amxd_status_t _bridging_check_management_port(amxd_object_t* object,
                                              amxd_param_t* param,
                                              amxd_action_t reason,
                                              const amxc_var_t* const args,
                                              amxc_var_t* const retval,
                                              void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    amxd_object_t* port = NULL;
    bool management_port_new;
    bool management_port_old;

    if(reason != action_param_validate) {
        rv = amxd_status_invalid_action;
        goto exit;
    }

    // checks if value can be converted to parameter type.
    rv = amxd_action_param_validate(object, param, reason, args, retval, priv);
    when_failed(rv, exit);

    management_port_new = amxc_var_dyncast(bool, args);
    management_port_old = amxc_var_dyncast(bool, &param->value);

    // We only want to check if ManagementPort is already set if we try to set it for this port
    // and when this port isn't already the management port
    if(management_port_new && !management_port_old) {
        port = amxd_object_findf(object, "^.[ManagementPort == true]");
        if(port != NULL) {
            SAH_TRACEZ_WARNING(ME, "this bridge already has a management port");
            rv = amxd_status_invalid_value;
            goto exit;
        }
        SAH_TRACEZ_INFO(ME, "%s is now the management port", object->name);
        rv = amxd_status_ok;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _lastchange_on_read(amxd_object_t* const object,
                                  UNUSED amxd_param_t* const param,
                                  amxd_action_t reason,
                                  UNUSED const amxc_var_t* const args,
                                  amxc_var_t* const retval,
                                  UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    port_info_t* port = NULL;
    uint32_t since_change = 0;
    uint32_t uptime = 0;

    if(reason != action_param_read) {
        SAH_TRACEZ_NOTICE(ME, "wrong reason, expected action_param_read(%d) got %d", action_param_read, reason);
        rv = amxd_status_invalid_action;
        goto exit;
    }

    // retval needs to be set, in case of error lastchange will be 0
    amxc_var_set_uint32_t(retval, 0);
    when_null_trace(object, exit, ERROR, "object can not be NULL");
    // when a parent object of port gets read,
    // this function will also be called for the template object
    when_null(object->priv, exit);
    port = (port_info_t*) object->priv;

    uptime = get_system_uptime();
    when_true_trace(uptime < port->last_change, exit, ERROR, "UpTime is smaller than last change");
    since_change = uptime - port->last_change;
    when_failed_trace(amxc_var_set_uint32_t(retval, since_change),
                      exit, ERROR, "failed to set parameter lastchange");

    rv = amxd_status_ok;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}
