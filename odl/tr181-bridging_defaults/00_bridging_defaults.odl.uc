%populate {
    object Bridging.Bridge {
{% let bridge_index=0 %}
{% for (let Bridge in BD.Bridges ) : %}
{% bridge_index++ %}
        instance add ("{{ lc(Bridge) }}") {
            parameter Enable = true;
            parameter Standard = "802.1Q-2011";
            object Port {
                instance add("{{ lc(Bridge) }}_bridge") {
                    parameter ManagementPort = true;
                    parameter Enable = true;
                    parameter Name = "{{BD.Bridges[Bridge].Name}}";
                }
{% let ports=[] %}
{% let i=-1 %}
{% for (let Port in BD.Bridges[Bridge].Ports) : %}
{% let alias=Port.Alias %}
{% if (Port.Vlan) : %}{% alias=Port.Alias + "_vlan" %}{% endif %}
{% if (alias in ports) : %}{% continue %}{% endif %}
{% i++ %}
{% ports[i]=alias %}
                instance add("{{alias}}") {
                    parameter Enable = true;
{% if (BDfn.isInterfaceWireless(Port.Name)) : %}
{% IntfIndex = BDfn.getInterfaceIndex(Port.Name, "wireless");
if (IntfIndex >= 0 ) : %}
                    parameter LowerLayers = "Device.WiFi.SSID.{{IntfIndex+1}}";
{% endif %}
                    parameter IsWireless = true;
                    parameter WirelessSectionName = "{{alias}}";
{% elif (BDfn.getInterfaceType(Port.Name) == "wds") : %}
{% IntfIndex = BDfn.getInterfaceIndex(Port.Name, "wds");
if (IntfIndex >= 0 ) : %}
                    parameter LowerLayers = "Device.WiFi.WDS.{{IntfIndex+1}}";
{% endif %}
{% elif (BDfn.isInterfaceMoca(Port.Name)) : %}
{% IntfIndex = BDfn.getInterfaceIndex(Port.Name, "moca");
if (IntfIndex >= 0 ) : %}
                    parameter LowerLayers = "Device.MoCA.Interface.{{IntfIndex+1}}";
{% endif %}
{% else %}
{% IntfIndex = BDfn.getInterfaceIndex(Port.Name, "ethernet");
if (IntfIndex >= 0 ) : %}
                    parameter LowerLayers = "Device.Ethernet.Interface.{{IntfIndex+1}}";
{% endif;%}
{% endif %}
{% if (Port.Vlan) : %}
                    parameter 'Type' = "CustomerVLANPort";
                    parameter 'ManagementPort' = false;
                    parameter 'AcceptableFrameTypes' = "AdmitOnlyVLANTagged";
{% endif %}
                }
{% endfor %}
            }
{% let vlans=[] %}
{% let i=-1 %}
{% for (let Port in BD.Bridges[Bridge].Ports) : if (Port.Vlan) : %}
{% if (i == -1) : %}
            object 'VLAN' {
{% endif %}
{% if (!(Port.Vlan in vlans)) : %}
{% i++ %}
{% vlans[i]=Port.Vlan %}
                instance add("VLAN-{{Port.Vlan}}") {
                    parameter 'Enable' = true;
                    parameter 'VLANID' = "{{Port.Vlan}}";
                    parameter 'Name' = "VLAN-{{Port.Vlan}}";
                }
{% endif %}
{% endif ; endfor %}
{% if (i != -1) : %}
            }
{% endif %}
{% let i=-1 %}
{% let j=-1 %}
{% for (let Port in BD.Bridges[Bridge].Ports) : %}
{% j++ %}
{% if (Port.Vlan) : %}
{% for (let q=0; q<length(vlans); q++) : if (vlans[q] == Port.Vlan) : %}{% VlanIndex = q+1 %}{% endif ; endfor %}
{% for (let q=0; q<length(ports); q++) : if (ports[q] == Port.Alias + "_vlan"): %}{% PortIndex = q+2 %}{% endif ; endfor %}
{% i++ %}
{% if (i == 0) : %}
            object 'VLANPort' {
{% endif %}
                instance add("vlan{{Port.Vlan}}_{{Port.Alias}}") {
                    parameter 'Enable' = true;
                    parameter 'Port' = "Device.Bridging.Bridge.{{bridge_index}}.Port.{{PortIndex}}";
                    parameter 'VLAN' = "Device.Bridging.Bridge.{{bridge_index}}.VLAN.{{VlanIndex}}";
                    parameter 'Untagged' = false;
                    parameter 'Name' = "{{Port.Name}}_{{Port.Vlan}}";
{% if (Port.VlanPriority): %}
                    parameter 'VlanPriority' = {{ Port.VlanPriority }};
{% else %}
                    parameter 'VlanPriority' = -1;
{% endif %}
                }
{% endif %}
{% endfor %}
{% if (i != -1) : %}
            }
{% endif %}
        }
{% endfor %}
    }
}
