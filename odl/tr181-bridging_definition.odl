

%define {
    /**
    * Layer 2 bridging configuration. Specifies bridges between different layer 2 interfaces.
    * Bridges can be defined to include layer 2 filter criteria to selectively bridge
    * traffic between interfaces.
    *
    * This object can be used to configure both 802.1D [802.1D-2004]
    * and 802.1Q [802.1Q-2011] bridges.
    *
    * Not all 802.1D and 802.1Q features are modeled, and some additional features not present
    * in either 802.1D or 802.1Q are modeled.
    *
    * 802.1Q [802.1Q-2011] bridges incorporate 802.1Q [802.1Q-2005] customer and
    * 802.1ad [802.1ad-2005] provider bridges.
    *
    * @version 1.0
    */
    %persistent object Bridging {
        /**
        * Only bridging modules that are added to this comma seperated list can be configured
        * in the Controller parameter under the bridge.
        * The name used should be the name of the so file without the extension.
        *
        * @version 1.0
        */
        %protected %read-only csv_string SupportedBridgeControllers = "mod-bridging-uci,mod-bridging-ioctl";

        /**
        * Only vlan modules that are added to this comma seperated list can be configured
        * in the Controller parameter under the bridge.
        * The name used should be the name of the so file without the extension.
        *
        * @version 1.0
        */
        %protected %read-only csv_string SupportedVLANControllers = "mod-vlan-ioctl";

        /**
        * Only STP modules that are added to this comma seperated list can be configured
        * in the Controller parameter under the bridge.
        * The name used should be the name of the so file without the extension.
        *
        * Note: to use mod-stp-sysfs, no stp daemon (or helper script /sbin/bridge-stp) can be installed.
        * To avoid installing the mstpd daemon, set CONFIG_SAH_AMX_TR181_BRIDGING_MSTPD=n
        *
        * @version 1.0
        */
        %protected %read-only csv_string SupportedSTPControllers = "mod-stp-sysfs,mod-stp-mstpd";

        /**
        * The maximum number of entries available in the Bridging.Bridge table.
        *
        * @version 1.0
        */
        %read-only uint32 MaxBridgeEntries = 10;

        /**
        * The maximum number of 802.1D [802.1D-2004] entries available in the Bridging.Bridge table.
        * A positive value for this parameter implies support for 802.1D.
        *
        * There is no guarantee that this many 802.1D Bridges can be configured.
        * For example, the CPE might not be able simultaneously to
        * support both 802.1D and 802.1Q Bridges.
        *
        * @version 1.0
        */
        %read-only uint32 MaxDBridgeEntries = 10;

        /**
        * The maximum number of 802.1Q [802.1Q-2011] entries available in the Bridging.Bridge table.
        * A non-zero value for this parameter implies support for 802.1Q.
        *
        * There is no guarantee that this many 802.1Q Bridges can be configured.
        * For example, the CPE might not be able simultaneously to
        * support both 802.1D and 802.1Q Bridges.
        *
        * @version 1.0
        */
        %read-only uint32 MaxQBridgeEntries = 10 ;

        /**
        * The maximum number of 802.1Q [802.1Q-2011] VLANs supported per Bridging.Bridge table entry.
        *
        * @version 1.0
        */
        %read-only uint32 MaxVLANEntries = 10;

        /**
        * Bridge table.
        *
        * At most one entry in this table can exist with a given value for Alias.
        * On creation of a new table entry, the Agent MUST choose an initial value for Alias such
        * that the new entry does not conflict with any existing entries. The non-functional key
        * parameter Alias is immutable and therefore MUST NOT change once it's been assigned.
        *
        * @version 1.0
        */
        %persistent object Bridge[] {
            on action add-inst call check_max_bridge_entries;
            on action add-inst call check_max_qbridge_entries;
            on action add-inst call check_max_dbridge_entries;
            on action destroy call bridging_bridge_removed;
            /**
            * Configures the module that should be used to apply this bridges configuration
            * can only be one of the supported controllers configured in Bridging.SupportedBridgeControllers
            *
            * @version 1.0
            */
            %persistent %protected string BridgeController = "mod-bridging-ioctl" {
                on action validate call check_is_empty_or_in "Bridging.SupportedBridgeControllers";
            }

            /**
            * Configures the module that should be used to apply this bridges configuration
            * can only be one of the supported controllers configured in Bridging.SupportedVLANControllers
            *
            * @version 1.0
            */
            %persistent %protected string VLANController = "mod-vlan-ioctl" {
                on action validate call check_is_empty_or_in "Bridging.SupportedVLANControllers";
            }

            /**
            * Configures the module that should be used to apply this bridge's STP configuration.
            * Can only be one of the supported controllers configured in Bridging.SupportedSTPControllers
            *
            * @version 1.0
            */
            %persistent %protected string STPController = "mod-stp-mstpd" {
                on action validate call check_is_empty_or_in "Bridging.SupportedSTPControllers";
            }

            /**
            * The number of entries in the Bridge table.
            *
            * @version 1.0
            */
            counted with BridgeNumberOfEntries;

            /**
            * Enables or disables this Bridge.
            *
            * @version 1.0
            */
            %persistent bool Enable;

            /**
            * The status of this Bridge. Enumeration of:
            *   Disabled
            *   Enabled
            *   Error
            *
            * The Error value MAY be used by the CPE to indicate a locally defined error condition.
            *
            * @version 1.0
            */
            %read-only string Status = "Disabled" {
                on action validate call check_enum
                    ["Disabled", "Enabled", "Error"];
            }

            /**
            * A non-volatile unique key used to reference this instance. Alias provides a mechanism
            * for a Controller to label this instance for future reference.
            *
            * The following mandatory constraints MUST be enforced:
            *   The value MUST NOT be empty.
            *   The value MUST start with a letter.
            *   If the value is not assigned by the Controller at creation time, the Agent MUST
            *   assign a value with an "cpe-" prefix.
            *
            * The value MUST NOT change once it's been assigned.
            *
            * @version 1.0
            */
            %persistent %unique %key string Alias {
                on action validate call check_maximum_length 64;
            }

            /**
            * Selects the standard supported by this Bridge table entry. Enumeration of:
            *   802.1D-2004 ([802.1D-2004])
            *   802.1Q-2005 ([802.1Q-2005])
            *   802.1Q-2011 (The Bridge provides support for at least one feature defined
            *   in [802.1Q-2011] that was not defined in [802.1Q-2005])
            *
            * Only one standard implemented at the moment
            * @version 1.0
            */
            %persistent string Standard = "802.1D-2004" {
                on action validate call check_enum
                    ["802.1D-2004", "802.1Q-2005", "802.1Q-2011"];
                on action validate call check_max_std_entries;
                userflags %odl-creation-param;
            }

            /**
            * The timeout period in seconds for aging out dynamically-learned forwarding information
            * as described in [Section 7.9.2/802.1Q-2011].
            *
            * The Dynamic Filtering Entries are not modeled. They are part of the bridge's internal Forwarding Database.
            *
            * @version 1.0
            */
            %persistent uint64 AgingTime = 300 {
                on action validate call check_range [10, 1000000];
            }

            /**
             * This function is designed to facilitate the addition of a (vlan)port to a bridge.
             * It checks for existing matching ports based on specified parameters, and it handles both normal ports and VLANPorts depending on the provided VlanId.
             *     - If the provided VlanId is 0 (on not provided), the function will add or enable a normal port interface.
             *     - If the VlanId is not 0, the function will add or enable a VLANPort. In this case, the VlanId is significant and will be used to identify the VLAN.
             *     - If a port with the requested lower layer already exists, it will be used. In this scenario, the requested alias will be ignored.
             * If adding a VLANPort, the bridge standard will be set to "802.1Q-2011".
             * @param LowerLayers: Path of an interface object that needs to be added to the bridge, will be  stacked immediately below the bridge interface
             * @param Alias: Optional, Alias for the port instance to be created
             * @param Enable: Should the interface be enabled or disabled after adding, default is false
             * @param VlanId: Optional, if set a vlan interface will be created on top of the interface referenced by lowerlayer
             * @param VlanName: Optional, name for the VLAN interface. If set without setting a vlanid, this value will be ignored.
             * @param VlanPriority: Optional, the egress priority for the vlan interface. If not set, the DefaultUserPriority value from the related Port will be used. 
             * @return amxd_status_ok if successful, error value otherwise
             */
            %protected void AddPort(%mandatory string LowerLayers, string Alias, bool Enable, uint32 VlanId, string VlanName, int32 VlanPriority=-1);

            /**
             * This function is designed to facilitate disabling a (vlan)port in a bridge.
             * If the VlandId is not 0, the function will look for a VLANPort to disable (the linked port and vlan instance will remain enabled)
             * Otherwise the function will look for a port instance and disable this one.
             * @param LowerLayers: The port with this lowerlayer will be disabled. If a vlanid is set, this value will be used to find the port interface associated to the vlan.
             * @param VlanId: Optional, if set, the vlan interface matching this vlan id will be disabled.
             * @return amxd_status_ok if successful, error value otherwise.
             */
            %protected void DisablePort(%mandatory string LowerLayers, uint32 VlanId);
        }
    }
}

include "tr181-bridging_port.odl";
include "tr181-bridging_vlan.odl";
include "tr181-bridging_vlanport.odl";
include "tr181-bridging_stp.odl";

%populate {
    // Bridge events
    on event "dm:instance-added" call bridging_bridge_added
        filter 'path == "Bridging.Bridge."';
    on event "dm:object-changed" call bridging_bridge_changed
        filter 'path matches "Bridging\.Bridge\.[0-9]+.$" && contains("parameters.Enable")';

    // port events
    on event "dm:instance-added" call bridging_port_added
        filter 'path matches "Bridging\.Bridge\.[0-9]+\.Port\.$"';
    on event "dm:object-changed" call bridging_managementport_changed
        filter 'path matches "Bridging\.Bridge\.[0-9]+\.Port\.[0-9]+" && contains("parameters.ManagementPort")';
    on event "dm:object-changed" call bridging_port_changed
        filter 'path matches "Bridging\.Bridge\.[0-9]+\.Port\.[0-9]+" && contains("parameters.Enable")';
    on event "dm:object-changed" call bridging_port_type_changed
        filter 'path matches "Bridging\.Bridge\.[0-9]+\.Port\.[0-9]+" && contains("parameters.Type")';
    on event "dm:object-changed" call bridging_port_default_priority_changed
        filter 'path matches "Bridging\.Bridge\.[0-9]+\.Port\.[0-9]+" && contains("parameters.DefaultUserPriority")';
    on event "dm:object-changed" call bridging_port_lower_layers_changed
        filter 'path matches "Bridging\.Bridge\.[0-9]+\.Port\.[0-9]+" && contains("parameters.LowerLayers")';
    on event "dm:object-changed" call bridging_port_not_implemented
        filter 'path matches "Bridging\.Bridge\.[0-9]+\.Port\.[0-9]+" &&
        (contains("parameters.PriorityTagging") || contains("parameters.IngressFiltering") || contains("parameters.PriorityRegeneration"))';
    on event "dm:instance-removed" call bridging_port_deleted
        filter 'path matches "Bridging\.Bridge\.[0-9]+\.Port\.$"';

    // vlanport events
    on event "dm:instance-added" call bridging_vlanport_added
        filter 'path matches "Bridging\.Bridge\.[0-9]+\.VLANPort\.$"';
    on event "dm:object-changed" call bridging_vlanport_enable_changed
        filter 'path matches "Bridging\.Bridge\.[0-9]+\.VLANPort\.[0-9]+" && contains("parameters.Enable")';
    on event "dm:object-changed" call bridging_vlanport_changed
        filter 'path matches "Bridging\.Bridge\.[0-9]+\.VLANPort\.[0-9]+" && (contains("parameters.Port") || contains("parameters.VLAN"))';
    on event "dm:object-changed" call bridging_vlanport_renamed
        filter 'path matches "Bridging\.Bridge\.[0-9]+\.VLANPort\.[0-9]+" && contains("parameters.Name")';
    on event "dm:object-changed" call bridging_vlanport_priority_changed
        filter 'path matches "Bridging\.Bridge\.[0-9]+\.VLANPort\.[0-9]+" && contains("parameters.VlanPriority")';

    // vlan events
    on event "dm:instance-removed" call bridging_vlan_deleted
        filter 'path matches "Bridging\.Bridge\.[0-9]+\.VLAN\.$"';
    on event "dm:object-changed" call bridging_vlan_id_changed
        filter 'path matches "Bridging\.Bridge\.[0-9]+\.VLAN\.[0-9]+" && contains("parameters.VLANID")';

    // STP events
    on event "dm:object-changed" call bridging_stp_changed
        filter 'path matches "Bridging\.Bridge\.[0-9]+\.STP\.$" && (contains("parameters.Enable") ||
                                                                    contains("parameters.BridgePriority") ||
                                                                    contains("parameters.HelloTime") ||
                                                                    contains("parameters.MaxAge") ||
                                                                    contains("parameters.ForwardingDelay"))';
    on event "dm:object-changed" call bridging_agingtime_changed
        filter 'path matches "Bridging\.Bridge\.[0-9]+.$" && contains("parameters.AgingTime")';
}
