/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__BRIDGE_MANAGER_UTILS_H__)
#define __BRIDGE_MANAGER_UTILS_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define str_empty(x) ((x == NULL) || (*x == '\0'))

char* trim_final_dot(const char* path);

bool vlanport_config_valid(vlanport_info_t* vlanport);
bool bridge_is_enabled(amxd_object_t* bridge_obj);
amxd_object_t* get_object_from_device_path(const char* device_path);
void mod_bridge_port_add(port_info_t* port);
void mod_bridge_port_remove(port_info_t* port);
void mod_bridge_vlanport_add(vlanport_info_t* vlanport);
void mod_bridge_vlanport_remove(vlanport_info_t* vlanport);

bridge_status_t bridge_check_and_apply_config(amxd_object_t* bridge_obj);

amxd_status_t bridge_info_new(bridge_info_t** bridge);
amxd_status_t bridge_info_clean(bridge_info_t** bridge);
amxd_status_t port_info_new(port_info_t** port, amxd_object_t* bridge, const char* netdev_intf);
amxd_status_t port_info_clean(port_info_t** port);
amxd_status_t vlanport_info_new(vlanport_info_t** vlanport,
                                amxd_object_t* vlanport_obj,
                                amxd_object_t* bridge_obj,
                                amxd_object_t* port_obj,
                                amxd_object_t* vlan_obj,
                                const char* vlanport_name);
amxd_status_t vlanport_info_clean(vlanport_info_t** vlanport);

int mod_bridge_disable(amxd_object_t* bridge_obj);
int mod_bridge_enable(amxd_object_t* bridge_obj);
int mod_bridge_add(amxd_object_t* bridge_obj);
int mngmnt_port_create_flag_query(port_info_t* port);
void mod_bridge_remove(amxd_object_t* bridge_obj);
void mod_configure_stp(amxd_object_t* bridge_obj, amxc_var_t* args);
void port_type(port_info_t* port, const char* type_str);
void set_lower_layer(amxd_object_t* bridge_obj);
uint32_t get_system_uptime(void);

#ifdef __cplusplus
}
#endif

#endif // __BRIDGE_MANAGER_UTILS_H__
