/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc.h>

#include "stp_sysfs.h"

#define ME "mod-stp"

#define BUF_SIZE 32
#define PATH_SIZE 100

static amxc_var_t* param_map;

static void init_param_map(void) {
    when_not_null_trace(param_map, exit, ERROR, "param_map already exists");

    amxc_var_new(&param_map);
    amxc_var_set_type(param_map, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, param_map, "HelloTime", "/sys/class/net/%s/bridge/hello_time");
    amxc_var_add_key(cstring_t, param_map, "BridgePriority", "/sys/class/net/%s/bridge/priority");
    amxc_var_add_key(cstring_t, param_map, "MaxAge", "/sys/class/net/%s/bridge/max_age");
    amxc_var_add_key(cstring_t, param_map, "ForwardingDelay", "/sys/class/net/%s/bridge/forward_delay");
    amxc_var_add_key(cstring_t, param_map, "Enable", "/sys/class/net/%s/bridge/stp_state");
    amxc_var_add_key(cstring_t, param_map, "AgingTime", "/sys/class/net/%s/bridge/ageing_time");

exit:
    return;
}

static void clean_param_map(void) {
    if(param_map != NULL) {
        amxc_var_delete(&param_map);
    }
}

static int file_read(const cstring_t path, char* buf, uint32_t size) {
    FILE* fp = NULL;
    int rv = -1;
    char* s = NULL;

    when_null(buf, exit);

    fp = fopen(path, "r");
    when_null_trace(fp, exit, ERROR, "Failed to open %s", path);
    s = fgets(buf, size, fp);
    when_null_trace(s, exit, ERROR, "Failed to read %s", path);

    rv = 0;
exit:
    if(fp != NULL) {
        fclose(fp);
        fp = NULL;
    }
    return rv;
}

static uint64_t file_read_uint64(const cstring_t path, char* buf, uint32_t size) {
    uint64_t result = 0;

    when_null(buf, exit);
    when_failed_trace(file_read(path, buf, size), exit, ERROR, "Failed to read file %s", path);

    result = strtoull(buf, NULL, 10);
exit:
    return result;
}

static void file_write(const cstring_t intf_name, const cstring_t file_path, long long unsigned int value) {
    char path[PATH_SIZE] = {0};
    char buf[BUF_SIZE] = {0};
    FILE* fp = NULL;
    uint64_t read_value = 0;
    int ret_val = -1;

    when_null_trace(intf_name, exit, ERROR, "Bad intf_name param given");
    when_null_trace(file_path, exit, ERROR, "Bad parameter id given, has an empty file_path");

    snprintf(path, sizeof(path), file_path, intf_name);

    read_value = file_read_uint64(path, buf, sizeof(buf));

    if(read_value != value) {
        fp = fopen(path, "w");
        when_null_trace(fp, exit, ERROR, "Failed to open file at path %s", path);

        ret_val = fprintf(fp, "%llu", value);
        when_false_trace(ret_val >= 0, exit, ERROR, "Failed to write to the file at path %s", path);
        SAH_TRACEZ_INFO(ME, "Succesfully written the value %llu to %s", value, path);
    } else {
        SAH_TRACEZ_INFO(ME, "No need to write %llu to %s", value, path);
    }
exit:
    if(fp != NULL) {
        fclose(fp);
        fp = NULL;
    }
    return;
}

static uint64_t stp_read_status(const char* bridge) {
    char path[PATH_SIZE] = {0};
    char buf[BUF_SIZE] = {0};

    snprintf(path, sizeof(path), "/sys/class/net/%s/bridge/stp_state", bridge);

    return file_read_uint64(path, buf, sizeof(buf));
}

static uint64_t convert_param_value(const char* key, uint64_t arg) {
    uint64_t ret = arg;

    when_null(key, exit);

    if((strcmp(key, "ForwardingDelay") == 0) || (strcmp(key, "AgingTime") == 0)) {
        ret = ret * 100;
    }

exit:
    return ret;
}

static int is_stp_enabled(UNUSED const char* function_name,
                          amxc_var_t* args,
                          amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const cstring_t bridge_name = GET_CHAR(args, "bridge_name");
    when_str_empty_trace(bridge_name, exit, ERROR, "Bridge name is empty, can't read STP status");

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, ret, "Enabled", (stp_read_status(bridge_name) != 0));

    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int modify_bridge_stp(UNUSED const char* function_name,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const cstring_t bridge_name = GET_CHAR(args, "bridge_name");
    when_str_empty_trace(bridge_name, exit, ERROR, "Bridge name is empty, can't configure STP");

    amxc_var_for_each(var, param_map) {
        const char* key = amxc_var_key(var);
        amxc_var_t* arg_var = amxc_var_get_key(args, key, AMXC_VAR_FLAG_DEFAULT);

        if(arg_var != NULL) {
            file_write(bridge_name, GET_CHAR(var, NULL), convert_param_value(key, GET_UINT64(arg_var, NULL)));
        }
    }

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, ret, "Status", (stp_read_status(bridge_name) != 0) ? "Enabled" : "Disabled");

    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static AMXM_CONSTRUCTOR stp_sysfs_start(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    init_param_map();
    when_failed_trace(amxm_module_register(&mod, so, MOD_STP_CTRL),
                      exit, ERROR, "Failed to register module %s", MOD_STP_CTRL);
    when_failed_trace(amxm_module_add_function(mod, "modify-bridge-stp", modify_bridge_stp),
                      exit, ERROR, "Failed to register function modify-bridge-stp");
    when_failed_trace(amxm_module_add_function(mod, "is-stp-enabled", is_stp_enabled),
                      exit, ERROR, "Failed to register function is-stp-enabled");
exit:
    return 0;
}

static AMXM_DESTRUCTOR stp_sysfs_stop(void) {
    clean_param_map();
    return 0;
}